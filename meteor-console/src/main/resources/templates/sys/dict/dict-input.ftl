<#--
/****************************************************
 * Description: 数据字典的输入页面，包括添加和修改
 * Copyright:   Copyright (c) 2019
 * Company:     ry
 * @author      reywong
 * @version     1.0
 * @see
	HISTORY
    *  2019-08-16 reywong Create File
**************************************************/
-->
<#include "/templates/xjj-index.ftl">

<@input url="${base}/sys/dict/save" id=tabId>
    <input type="hidden" name="id" value="${dict.id}"/>
    <@formgroup title='属组'>
        <input type="text" id="groupCode" name="groupCode" value="${dict.groupCode}" check-type="required">

    </@formgroup>

    <@formgroup title='编码'>
        <input type="text" name="code" value="${dict.code}" check-type="required">

    </@formgroup>

    <@formgroup title='值'>
        <input type="text" name="name" value="${dict.name}" check-type="required">

    </@formgroup>
    <@formgroup title='序号'>
        <input type="text" name="sn" value="${dict.sn}" check-type="number">

    </@formgroup>
    <@formgroup title='备注'>
        <input type="text" name="detail" value="${dict.detail}">

    </@formgroup>

    <@formgroup title='状态'>
        <@swichInForm name="status" val=dict.status onTitle="有效" offTitle="无效"></@swichInForm>

    </@formgroup>
</@input>
<script type="text/javascript">
    $(function () {
        var tag_data = new Array();
        <#if groupList?exists>
        <#list groupList as p>
        tag_data.push({"id": "${p.groupCodeM}", "value": "${p.groupName}"});
        </#list>
        </#if>
        $('#groupCode').selectPage({
            showField: 'value',
            keyField: 'id',
            data: tag_data,
            //格式化显示项目，提供源数据进行使用
            formatItem: function (data) {
                return data.value + '(' + data.id + ')';
            }
        });
    });


</script>
