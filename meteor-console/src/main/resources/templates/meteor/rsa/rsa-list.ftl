<#--
/****************************************************
 * Description: 私钥信息的简单列表页面，没有编辑功能
 * Copyright:   Copyright (c) 2019
 * Company:     ry
 * @author      reywong
 * @version     1.0
 * @see
	HISTORY
    *  2019-12-13 reywong Create File
**************************************************/
-->
<#include "/templates/xjj-list.ftl">
<@list id=tabId>
    <thead>
    <tr>
        <th><input type="checkbox" class="bscheckall"></th>
        <th>私钥名称</th>
        <th>私钥用户</th>
        <th>私钥密码</th>
        <th>状态</th>
        <th>创建时间</th>
        <th>创建人员ID</th>
        <th>创建人员姓名</th>
        <th>操作</th>
    </tr>
    </thead>
    <tbody>
    <#list page.items?if_exists as item>
        <tr>
            <td>
                <input type="checkbox" class="bscheck" data="id:${item.id}">
            </td>
            <td>
                ${item.rsaName}

            </td>
            <td>
                ${item.rsaUsername}

            </td>
            <td>
                ${item.rsaPassword}

            </td>
            <td>
                <span class="label <#if item.status=XJJConstants.COMMON_STATUS_VALID>label-info</#if> arrowed-in arrowed-in-right">${XJJDict.getText(item.status)}</span>

            </td>
            <td>
                ${item.createTime?string('yyyy-MM-dd HH:mm:ss')}
            </td>
            <td>
                ${item.createPersonId}

            </td>
            <td>
                ${item.createPersonName}

            </td>
            <td>
                <@button type="purple" icon="fa fa-pencil" onclick="XJJ.edit('${base}/meteor/rsa/input/${item.id}','修改私钥信息','${tabId}');">修改</@button>
                <@button type="danger" icon=" fa fa-trash-o" onclick="XJJ.del('${base}/meteor/rsa/delete/${item.id}','删除私钥信息？',false,{id:'${tabId}'});">删除</@button>
            </td>
        </tr>
    </#list>
    </tbody>
</@list>
