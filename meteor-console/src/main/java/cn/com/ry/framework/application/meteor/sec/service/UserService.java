package cn.com.ry.framework.application.meteor.sec.service;

import cn.com.ry.framework.application.meteor.framework.exception.ValidationException;
import cn.com.ry.framework.application.meteor.sec.entity.XjjUser;
import cn.com.ry.framework.application.meteor.framework.service.XjjService;

import java.util.Map;

public interface UserService  extends XjjService<XjjUser> {
	/**
	 * 导入
	 * @param fileId
	 */
    public Map<String,Object> saveImportUser(Long fileId) throws ValidationException;
}
