package cn.com.ry.framework.application.meteor.framework.configuration;

import java.util.Properties;

public interface ConfigLoader{
	/**
	 * Load Config
	 */
	public Properties load() throws Exception;

	public void save(Properties properties) throws Exception;
}
