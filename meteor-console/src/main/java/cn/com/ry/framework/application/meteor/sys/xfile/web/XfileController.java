/****************************************************
 * Description: Controller for t_sys_xfile
 * Copyright:   Copyright (c) 2018
 * Company:     xjj
 * @author      RY
 * @version     1.0
 * @see
	HISTORY
	*  2018-05-04 RY Create File
**************************************************/
package cn.com.ry.framework.application.meteor.sys.xfile.web;

import cn.com.ry.framework.application.meteor.framework.exception.ValidationException;
import cn.com.ry.framework.application.meteor.framework.json.XjjJson;
import cn.com.ry.framework.application.meteor.framework.security.annotations.*;
import cn.com.ry.framework.application.meteor.sys.xfile.entity.XfileEntity;
import cn.com.ry.framework.application.meteor.sys.xfile.service.XfileService;
import cn.com.ry.framework.application.meteor.framework.web.SpringControllerSupport;
import cn.com.ry.framework.application.meteor.framework.web.support.Pagination;
import cn.com.ry.framework.application.meteor.framework.web.support.QueryParameter;
import cn.com.ry.framework.application.meteor.framework.web.support.XJJParameter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.Date;

@Controller
@RequestMapping("/sys/xfile")
public class XfileController extends SpringControllerSupport {
	@Autowired
	private XfileService xfileService;


	@SecPrivilege(title="文件管理")
	@RequestMapping(value = "/index")
	public String index(Model model) {
		String page = this.getViewPath("index");
		return page;
	}

	@SecList
	@RequestMapping(value = "/list")
	public String list(Model model,
			@QueryParameter XJJParameter query,
			@ModelAttribute("page") Pagination page
			) {
		page = xfileService.findPage(query,page);
		return getViewPath("list");
	}

	@SecCreate
	@RequestMapping("/input")
	public String create(@ModelAttribute("xfile") XfileEntity xfile,Model model){
		return getViewPath("input");
	}

	@SecEdit
	@RequestMapping("/input/{id}")
	public String edit(@PathVariable("id") Long id, Model model){
		XfileEntity xfile = xfileService.getById(id);
		model.addAttribute("xfile",xfile);
		return getViewPath("input");
	}

	@SecCreate
	@SecEdit
	@RequestMapping("/save")
	public @ResponseBody
    XjjJson save(@ModelAttribute XfileEntity xfile){

		validateSave(xfile);
		if(xfile.isNew())
		{
			xfile.setCreateDate(new Date());
			xfileService.save(xfile);
		}else
		{
			xfileService.update(xfile);
		}
		return XjjJson.success("保存成功");
	}


	/**
	 * 数据校验
	 **/
	private void validateSave(XfileEntity xfile){
		//必填项校验
		// 判断file_realname是否为空
		if(null==xfile.getFileRealname()){
			throw new ValidationException("校验失败，file_realname不能为空！");
		}
		// 判断file_path是否为空
		if(null==xfile.getFilePath()){
			throw new ValidationException("校验失败，file_path不能为空！");
		}
		// 判断file_title是否为空
		if(null==xfile.getFileTitle()){
			throw new ValidationException("校验失败，file_title不能为空！");
		}
		// 判断create_date是否为空
		if(null==xfile.getCreateDate()){
			throw new ValidationException("校验失败，create_date不能为空！");
		}
	}

	@SecDelete
	@RequestMapping("/delete/{id}")
	public @ResponseBody XjjJson delete(@PathVariable("id") Long id){
		xfileService.delete(id);
		return XjjJson.success("成功删除1条");
	}
	@SecDelete
	@RequestMapping("/delete")
	public @ResponseBody XjjJson delete(@RequestParam("ids") Long[] ids){
		if(ids == null || ids.length == 0){
			return XjjJson.error("没有选择删除记录");
		}
		for(Long id : ids){
			xfileService.delete(id);
		}
		return XjjJson.success("成功删除"+ids.length+"条");
	}
}

