/****************************************************
 * Description: ServiceImpl for 用户角色
 * Copyright:   Copyright (c) 2018
 * Company:     xjj
 * @author RY
 * @version 1.0
 * @see
HISTORY
 *  2018-04-18 RY Create File
 **************************************************/

package cn.com.ry.framework.application.meteor.sec.service.impl;

import cn.com.ry.framework.application.meteor.framework.dao.XjjDAO;
import cn.com.ry.framework.application.meteor.framework.service.XjjServiceSupport;
import cn.com.ry.framework.application.meteor.sec.dao.UserRoleDao;
import cn.com.ry.framework.application.meteor.sec.entity.UserRoleEntity;
import cn.com.ry.framework.application.meteor.sec.service.UserRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
public class UserRoleServiceImpl extends XjjServiceSupport<UserRoleEntity> implements UserRoleService {

    @Autowired
    private UserRoleDao userRoleDao;

    @Override
    public XjjDAO<UserRoleEntity> getDao() {

        return userRoleDao;
    }

    @Transactional
    public void deleteAndSave(Long userId, Long[] roleIds) {
        //删除
        userRoleDao.deleteByUserId(userId);
        if (null != roleIds) {
            for (int i = 0; i < roleIds.length; i++) {
                UserRoleEntity userRole = new UserRoleEntity();
                userRole.setUserId(userId);
                userRole.setRoleId(roleIds[i]);
                userRoleDao.save(userRole);
            }
        }
    }

    public void deleteBy2Id(Long userId, Long roleId) {
        userRoleDao.deleteBy2Id(userId, roleId);
    }
}
